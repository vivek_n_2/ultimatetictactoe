import time

class Player45:

    def __init__(self):
        self.grid = {'00': ['01', '10'],
         '01': ['00', '02'],
         '02': ['01', '12'],
         '10': ['00', '20'],
         '11': ['11'],
         '12': ['02', '22'],
         '20': ['10', '21'],
         '21': ['20', '22'],
         '22': ['12', '21']}
        self.stime = 0
        self.maxdepth = 5



    def PossibleMoves(self, board, block, old_move):
        if old_move == (-1, -1):
            valid_moves = []
            for i in range(9):
                for j in range(9):
                    valid_moves.append((i, j))


            return valid_moves
        x = old_move[0] % 3
        y = old_move[1] % 3
        arr = self.grid[(str(x) + str(y))]
        valid_moves = []
        for k in arr:
            X = int(k[0])
            Y = int(k[1])
            PossibleBlock = 3 * X + Y
            if block[PossibleBlock] != '-':
                continue
            for i in range(3):
                for j in range(3):
                    if board[(3 * X + i)][(3 * Y + j)] == '-':
                        valid_moves.append([3 * X + i, 3 * Y + j])



        if len(valid_moves) == 0:
            for X in range(3):
                for Y in range(3):
                    PossibleBlock = 3 * X + Y
                    if block[PossibleBlock] != '-':
                        continue
                    for i in range(3):
                        for j in range(3):
                            if board[(3 * X + i)][(3 * Y + j)] == '-':
                                valid_moves.append([3 * X + i, 3 * Y + j])

        return valid_moves



    def check_win(self, board, moveX, moveY, cur):
        if board[moveX][0] == cur and board[moveX][1] == cur and board[moveX][2] == cur:
            return cur
        else:
            if board[0][moveY] == cur and board[1][moveY] == cur and board[2][moveY] == cur:
                return cur
            if moveX == moveY and board[0][0] == cur and board[1][1] == cur and board[2][2] == cur:
                return cur
            if moveX + moveY == 2 and board[0][2] == cur and board[1][1] == cur and board[2][0] == cur:
                return cur
            for i in range(3):
                for j in range(3):
                    if board[i][j] == '-':
                        return '-'

            return 'D'



    def terminaltest(self, block, player):

        fin = 0
        for i in block:
            if i != '-':
                fin += 1

        if fin == 9 :
            return True
        
        row = [True] * 3
        col = [True] * 3
        diag = [True] * 2
        for i in range(3):
            for j in range(3):
                if block[(3 * i + j)] != player:
                    row[i] = False
                    col[j] = False
                    if i + j == 2:
                        diag[1] = False
                    if i == j:
                        diag[0] = False


        return row[0] or row[1] or row[2] or col[0] or col[1] or col[2] or diag[0] or diag[1]

    def update_utility(self,myc,opc,utility):

        if myc == 2 and opc == 0:
            utility += 10
        if myc == 0 and opc == 2:
            utility -= 10
        if myc == 1 and opc == 0:
            utility += 1
        if myc == 0 and opc == 1:
            utility -= 1

        return utility

  
    def calucate_multiplier(self,sum):

        factor_f = 0
        if -3 < sum and sum < -2:
            factor_f = -90
        elif -2 < sum and sum < -1:
            factor_f = -9
        elif 0 < sum and sum < 1:
            factor_f = 1
        elif -1 < sum and sum < 0:
            factor_f = -1
        elif 1 < sum and sum < 2:
            factor_f = 9
        elif 2 < sum and sum < 3:
            factor_f = 90
        return factor_f

    def sum_relative(self, ex, ey, utility, board, flag):

        me = flag
        op = 'x'
        if me == 'x':
            op = 'o'

        myrc = 0
        mycc = 0
        myd1c = 0
        myd2c = 0

        oprc = 0
        opcc = 0
        opd1c = 0
        opd2c = 0

        max_line_utility = 0

        startX = (ex//3)*3
        startY = (ey//3)*3

        for i in range(3):
            # in the corresponding column
            if board[startX+i][ey] == me:
                mycc+=1
            elif board[startX+i][ey] == op:
                opcc+=1
            # in corresponding row
            if board[ex][startY+i] == me:
                myrc+=1
            elif board[ex][startY+i] == op:
                oprc+=1
            # left diagonal
            if ex%3==ey%3:
                if board[startX+i][startY+i] == me:
                    myd1c+=1
                elif board[startX+i][startY+i] == op:
                    opd1c+=1
            # right diagonal
            if ex%3+ey%3 == 2:
                if board[startX+i][startY+2-i] == me:
                    myd2c+=1
                elif board[startX+i][startY+2-i] == op:
                    opd2c+=1

        if self.update_utility(myrc,oprc,utility) > 0 :
            return True
        mycc += myrc
        opcc += oprc
        if self.update_utility(mycc,opcc,utility) > 0:
            return True
        myd1c += mycc
        opd1c += opcc
        if self.update_utility(myd1c,opd1c,utility) > 0:
            return True
        myd2c += myd1c
        opd2c += opd1c
        if self.update_utility(myd2c,opd2c,utility) > 0:
            return True
        return False

    def get_utility(self,board,block_index,flag):
        me = flag
        op = 'x'
        if me == 'x':
            op = 'o'

        multiplier = [1,10,100]

        startX = (block_index//3)*3
        startY = (block_index%3)*3

        myrc = [0]*3
        mycc = [0]*3
        mydc = [0]*2

        oprc = [0]*3
        opcc = [0]*3
        opdc = [0]*2

        utility = 0

        myc = 0
        opc = 0
        rx = 0
        ry = 0
        for i in range(3):
            for j in range(3):
                if board[startX+i][startY+j] == me:
                    myc+=1
                    myrc[i]+=1
                    mycc[j]+=1
                    if i==j:
                        mydc[0]+=1
                    if i+j==2:
                        mydc[1]+=1
                elif board[startX+i][startY+j] == op:
                    opc+=1
                    oprc[i]+=1
                    opcc[j]+=1
                    if i==j:
                        opdc[0]+=1
                    if i+j==2:
                        opdc[1]+=1
                else:
                    rx = startX+i
                    ry = startY+j

        flag = 1
        for i in range(3):
            if flag == 0:
              continue
            
            if myrc[i] == 3:
                utility = 100  
                flag = 0
                break
            elif oprc[i] == 3:
                utility = -100
                flag = 0
                break
            else:
                utility = self.update_utility(myrc[i],oprc[i],utility)

        for i in range(3):

            if flag == 0:
              continue
            
            if mycc[i] == 3:
                utility = 100  
                flag = 0
                break
            elif opcc[i] == 3:
                utility = -100
                flag = 0
                break
            else:
                utility = self.update_utility(mycc[i],opcc[i],utility)

        if flag == 1:
            if mydc[0] == 3:
                utility = 100
            elif opdc[0] == 3:
                utility = -100
            else:
                utility = self.update_utility(mydc[0],opdc[0],utility)

        if flag == 1:
            if mydc[1] == 3:
                utility = 100
            elif opdc[1] == 3:
                utility = -100
            else:
                utility = self.update_utility(mydc[1],opdc[1],utility)



        if utility == 0 :

            if myc+opc == 9:
                return "DRAW"
            elif myc+opc == 8 and not self.sum_relative(rx,ry,utility,board,flag):
                return "DRAW"
        
        return utility

    def check_clashes(self,index,block_utilities):
        i = index
        flag=0
        for j in range(0,3):
            if(index<3 and (block_utilities[i][j]==1 or block_utilities[i][j]==-1)):
                for k in range(j+1,3):
                    if(block_utilities[i][k]==-block_utilities[i][j]):
                        flag=1
                        break
            elif(index>2 and index<6 and (block_utilities[j][i-3]==1 or block_utilities[j][i-3]==-1)):
                for k in range(j,3):
                    if(block_utilities[k][i-3]==-block_utilities[j][i-3]):
                        flag=1
                        break
            elif(index==6 and (block_utilities[j][j]==1 or block_utilities[j][j]==-1)):
                for k in range(j,3):
                    if(block_utilities[k][k]==-block_utilities[j][j]):
                        flag=1
                        break
            elif(index==7 and (block_utilities[j][2-j]==1 or block_utilities[j][2-j]==-1)):
                for k in range(j,3):
                    if(block_utilities[k][2-k]==-block_utilities[j][2-j]):
                        flag=1
                        break
        return flag


    def heuristic(self,board,block,flag):
        me = flag
        op = 'x'
        if flag == 'x':
            op = 'o'
        block_utilities = [ [0 for i in range(3)] for j in range(3)]
        line_utilities = [0 for i in range(9)]

        for block_index in range(9):
            blockX = block_index//3
            blockY = block_index%3
            block_utilities[blockX][blockY] = self.get_utility(board,block_index,flag)

            if block_utilities[blockX][blockY] != "DRAW":
                block_utilities[blockX][blockY] /= 100.0
        #print "Mine",block_utilities

        #print block_utilities

        multiplier = [1,10,100]
        

        Final_utility = 0
        mywon = 0
        opwon = 0
        myfav = 0
        opfav = 0

        for i in range(3):
            rsum = 0
            csum = 0
            d1sum = 0
            d2sum = 0

            rdraw = False
            cdraw = False
            d1draw = False
            d2draw = False

            victory = '-'

            for j in range(3):
                # row
                if block_utilities[i][j] == "DRAW":
                    rdraw = True
                else:
                    rsum += block_utilities[i][j]

                # col
                if block_utilities[j][i] == "DRAW":
                    cdraw = True
                else:
                    csum += block_utilities[j][i]

                #daig1
                if block_utilities[j][j] == "DRAW":
                    d1draw = True
                else:
                    d1sum += block_utilities[j][j]

                #diag2
                if block_utilities[j][2-j] == "DRAW":
                    d2draw = True
                else:
                    d2sum += block_utilities[j][2-j]
    

                if block_utilities[i][j] == 1:
                    mywon += 1
                elif block_utilities[i][j] == -1:
                    opwon += 1

                factor_r = self.calucate_multiplier(rsum)
                factor_c = self.calucate_multiplier(csum)
                factor_d1 = self.calucate_multiplier(d1sum)
                factor_d2 = self.calucate_multiplier(d2sum)

            """
                modified
            """

            #print "sums at mine for ",i,"is",rsum,csum,d1sum,d2sum
            if rsum == 1 or rsum == -1:
                rdraw |= self.check_clashes(i,block_utilities)
            elif csum == 1 or csum == -1:
                cdraw |= self.check_clashes(i+3,block_utilities)
            elif d1sum == 1 or d1sum == -1:
                d1draw |= self.check_clashes(6,block_utilities)
            elif d2sum == 1 or d2sum == -1:
                d2draw |= self.check_clashes(7,block_utilities)

            if (rsum == 3) or (csum == 3):
                victory = me
            elif (rsum == -3) or (csum == -3):
                victory = op
            elif (d1sum == 3) or (d2sum == 3):
                victory = me
            elif (d1sum == -3) or (d2sum == -3):
                victory = op

            if victory != '-':
                break
            #print "mine at",i,"is",rdraw,cdraw,d1draw,d2draw
            if rdraw:
                line_utilities[i] = 0
            else: 
                line_utilities[i] = int(pow(10, abs(int(rsum)) - 1)) + (rsum - int(rsum)) * factor_r
                if rsum < 0:
                    line_utilities[i] *= -1

            if cdraw:
                line_utilities[i+3] = 0
            else:
                line_utilities[i+3] = int(pow(10, abs(int(csum)) - 1)) + (csum - int(csum)) * factor_c
                if csum < 0:
                    line_utilities[i+3] *= -1

            if d1draw:
                line_utilities[6] = 0
            else:
                line_utilities[6] = int(pow(10, abs(int(d1sum)) - 1)) + (d1sum - int(d1sum)) * factor_d1
                if d1sum < 0:
                    line_utilities[6] *= -1

            if d2draw:
                line_utilities[7] = 0
            else:
                line_utilities[7] = int(pow(10, abs(int(d2sum)) - 1)) + (d2sum - int(d2sum)) * factor_d2
                if d2sum < 0:
                    line_utilities[7] *= -1



        for i in range(3):
            """
                I included the middle cell
            
            if i ==1 :
                continue
                """
            if block_utilities[i][i] != "DRAW":
                if block_utilities[i][i] > 0:
                    myfav += block_utilities[i][i]
                else:
                    opfav -= block_utilities[i][i]
        for i in range(3):

            if i==1:
                continue
            if block_utilities[i][2-i] != "DRAW":
                if block_utilities[i][2-i] > 0:
                    myfav += block_utilities[i][2-i]
                else:
                    opfav -= block_utilities[i][2-i]

        if victory != '-':
            if victory == me:
                Final_utility = 100
            else:
                Final_utility = -100
        else:
            for i in range(8):
                Final_utility += line_utilities[i]

            if me == 'x' and abs(Final_utility) > 30.0:
                Final_utility += (mywon-opwon)*10
            if me == 'o' and abs(Final_utility) < 10.0:
                Final_utility += (mywon-opwon)*10
            Final_utility += (myfav-opfav)*8

        return Final_utility

    def update(self, board, block, move, player):
        temp_board = []
        for i in range(9):
            temp = []
            for j in range(9):
                temp.append(board[i][j])

            temp_board.append(temp)

        temp_board[move[0]][move[1]] = player
        temp_block = []
        for i in range(9):
            temp_block.append(block[i])

        tiny = []
        startX = move[0] // 3 * 3
        startY = move[1] // 3 * 3
        for i in range(startX, startX + 3):
            temp = []
            for j in range(startY, startY + 3):
                temp.append(temp_board[i][j])

            tiny.append(temp)

        block_index = startX + move[1] // 3
        temp_block[block_index] = self.check_win(tiny, move[0] % 3, move[1] % 3, player)
        return (temp_board, temp_block)



    def alphabeta(self, board, block, old_move, flag, depth, alpha, beta, maximizingPlayer):
        global completed
        me = flag
        op = 'x'
        if me == 'x':
            op = 'o'
        if time.time() - self.stime >= 11.6:
            completed = False
            return depth,self.heuristic(board,block,flag)
        if depth == self.maxdepth or self.terminaltest(block, 'x') or self.terminaltest(block, 'o'):
            #print old_move,depth,self.heuristic(board,block,flag)
            return depth,self.heuristic(board,block,flag)
        next_moves = self.PossibleMoves(board, block, old_move)
        bestchild = None
        bestlevel = 100
        if maximizingPlayer:
            utility = -1000
            #if depth == 0:
            #    print next_moves
            for child in next_moves:
                (temp_board, temp_block,) = self.update(board, block, child, me)
                (reachlevel, child_utility,) = self.alphabeta(temp_board, temp_block, child, flag, depth + 1, alpha, beta, False)
                if child_utility > utility:
                    utility = child_utility
                    bestlevel = reachlevel
                    bestchild = child
                elif child_utility == utility and reachlevel < bestlevel:
                    bestlevel = reachlevel
                    bestchild = child
                alpha = max(alpha, utility)
                if beta <= alpha:
                    break

            if depth > 0:
                return (bestlevel, utility)
            else:
                return bestchild
        else:
            utility = 1000
            for child in next_moves:
                (temp_board, temp_block,) = self.update(board, block, child, op)
                (reachlevel, child_utility,) = self.alphabeta(temp_board, temp_block, child, flag, depth + 1, alpha, beta, True)
                if child_utility < utility:
                    utility = child_utility
                    bestchild = child
                elif child_utility == utility and reachlevel < bestlevel:
                    bestlevel = reachlevel
                    bestchild = child
                beta = min(beta, utility)
                if beta <= alpha:
                    break

            return (bestlevel, utility)



    def move(self, board, block, old_move, flag):
        global completed
        self.stime = time.time()
        self.maxdepth = 5
        #print self.heuristic(board,block,flag)
        #print heuristic.check(board, block, flag)
        #return "END"
        mymove = tuple(self.alphabeta(board, block, old_move, flag, 0, -1000, 1000, True))
        for self.maxdepth in range(6, -1):
            completed = True
            retmove = tuple(self.alphabeta(board, block, old_move, flag, 0, -1000, 1000, True))
            if completed:
                mymove = retmove
            else:
                self.maxdepth -= 1
                break

        #print 'Reached a depth of',
        #print self.maxdepth
        #print 'Time taken',
        #print time.time() - self.stime
        return mymove



if __name__ == '__main__':
    a = Player45()
    flag = 'x'
    old_move = (0, 0)
    block = ['-',
     'x',
     '-',
     'x',
     'o',
     'x',
     'x',
     'o',
     'x']

    board = [ ['o', 'x', 'o', '-', '-', '-', '-', 'o', 'o'] ,
['o', 'x', 'x', '-', '-', '-', '-', '-', 'o'] ,
['-', 'o', 'x', 'x', 'x', 'x', '-', 'o', '-'] ,
['x', '-', 'o', 'x', '-', '-', 'o', '-', '-'] ,
['-', 'x', '-', '-', '-', '-', 'x', 'x', 'x'] ,
['o', '-', 'x', 'o', 'o', 'o', '-', '-', 'o'] ,
['x', 'o', 'o', 'o', '-', 'x', '-', 'x', '-'] ,
['x', '-', 'o', 'o', '-', '-', 'x', 'x', 'x'] ,
['x', '-', '-', 'o', '-', '-', '-', 'o', '-'] ,
]
    print a.move(board, block, old_move, flag)

